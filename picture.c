
void closePicture() {
  gtk_widget_destroy(GTK_WIDGET(pictureLayout));
  loggedIn();
}

void showPicture() {
  gtk_widget_destroy(GTK_WIDGET(loggedinLayout));
  pictureLayout = createLayout();
  GtkWidget *titleLabel = gtk_label_new("Picture");
  gtk_layout_put(GTK_LAYOUT(pictureLayout), GTK_WIDGET(titleLabel), 0, 0);
  gtk_widget_show(GTK_WIDGET(titleLabel));
  // back button
  createImage("back.png", GTK_LAYOUT(pictureLayout), 64, 64, 0, 0, 1);
  createClickEvent(GTK_LAYOUT(pictureLayout), 64, 64, 0, 0, closePicture);
  // show layout
  gtk_widget_show(GTK_WIDGET(pictureLayout));
}
