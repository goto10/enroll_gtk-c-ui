#include <curl/curl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
​ char resource_base_path = "./resources/";
char *resource_list[] = {"screensaver.png"};
const char ws_url = "https://gymportal.internallab.co.uk/";
​ int get_pi_serial();
int check_resources();
int get_resources();
​ int main() {
  ​
  // Check Get Resources
  get_resources();
  ​
}
​ int get_pi_serial() {
  FILE *f = fopen("/proc/cpuinfo", "r");
  if (!f) {
    return;
  }

  char line[256];
  int serial;
  while (fgets(line, 256, f)) {
    if (strncmp(line, "Serial", 6) == 0) {
      char serial_string[16 + 1];
      serial = atoi(strcpy(serial_string, strchr(line, ':') + 2));
    }
  }
  ​ fclose(f);
  return serial;
}
​ int get_resources() {
  ​ int resources_exist;
  ​ ​
      // Get Pi Serial Number
      int serial = get_pi_serial();
  ​
      // Check If Resources Exist
      resources_exist = check_resources(serial);
  ​ if (resources_exist != 1) {
    ​
        // Retrieve Resources
        for (int i = 0; i < sizeof(resource_list); i++) {
      ​ http_get_file(resource_list[i], serial);
      ​
    }
  }
  ​
}
​ int check_resources() {
  ​ int exist = 1;
  ​ for (int i = 0; i < sizeof(resource_list); i++) {
    ​ if (access(resource_list[i], F_OK) == -1) { exist = 0; }
    ​
  }

  return exist;
}
​ int http_get_file(char *file_name, char ws, int serial) {
  ​ char full_path[PATH_MAX] = build_path(resource_base_path, file_name);
  char ws_url[1024];
  strcat(ws_url, ws_url);
  strcat(ws_url, ws);
  strcat(ws_url, "?serial=");
  strcat(ws_url, serial);
  strcat(ws_url, "&file=");
  strcat(ws_url, file_name);
  ​ FILE *file = fopen(full_path, "w");
  ​ curl = curl_easy_init();
  ​ if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, ws_url);
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
    curl_easy_perform(curl);
    curl_easy_cleanup(easyhandle);
    ​
  }
  else {
    fclose(file);
    return 0;
  }
  fclose(file);
  return 1;
}
​ char build_path(char base, char *file) {
  ​ char full_path[PATH_MAX];
  strcat(full_path, base);
  strcat(full_path, file);
  ​ return full_path;
}