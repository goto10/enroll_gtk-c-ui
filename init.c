#define WINDOW_INIT_WIDTH 400
#define WINDOW_INIT_HEIGHT 600

#define BUTTONS_X_POS 20
#define BUTTONS_Y_POS 20

#define X_STEP 80
#define Y_STEP 30

#define MAX_KEY_TEMPLATE_SIZE (15)

#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif

char *ImageDirName = "./Img/";
char *TemplatesDirName = "./Templates/";

char *ImageDefExt = ".bmp";
char *TemplateDefEx = ".tmpl";

char TabErr[][64] = {
    "No errors",          // FTR_RETCODE_OK
    "Unknown error code", // correspond FTR_RETCODE_ERROR_BASE value,
                          // unlegal code
    "No memory",          // FTR_RETCODE_NO_MEMORY
    "Invalid argument function call",       // FTR_RETCODE_INVALID_ARG
    "Repetition use some object (service)", // FTR_RETCODE_ALREADY_IN_USE
    "Sample is not correspond purpose",     // FTR_RETCODE_INVALID_PURPOSE
    "Internal SDK error",                   // FTR_RETCODE_INTERNAL_ERROR

    // Information return codes (may be)
    "Unable to capture",               // FTR_RETCODE_UNABLE_TO_CAPTURE
    "Operation canceled by User",      // FTR_RETCODE_CANCELED_BY_USER
    "Number of retries is overflow",   // FTR_RETCODE_NO_MORE_RETRIES
    "",                                // reserved
    "Source sampling is inconsistent", // FTR_RETCODE_INCONSISTENT_SAMPLING
    "Trial is expired",                // FTR_RETCODE_TRIAL_EXPIRED
    "Not enough minutiae"              // FTR_RETCODE_NOT_ENOUGH_MINUTIAE

};

// Table of device return codes
char TabDev[][64] = {
    "Unknown error code",       // correspond FTR_RETCODE_DEVICE_BASE value,
                                // unlegal code
    "Frame source not set",     // FTR_RETCODE_FRAME_SOURCE_NOT_SET
    "Device not connected",     // FTR_RETCODE_DEVICE_NOT_CONNECTED
    "Device failure",           // FTR_RETCODE_DEVICE_FAILURE
    "Empty frame was returned", // FTR_RETCODE_EMPTY_FRAME
    "Fake finger was detected", // FTR_RETCODE_FAKE_SOURCE
    "Incompatible hardware detected", // FTR_RETCODE_INCOMPATIBLE_HARDWARE
    "Upgrade the device firmware",    // FTR_RETCODE_INCOMPATIBLE_FIRMWARE
    "Frame source has been changed"   // FTR_RETCODE_FRAME_SOURCE_CHANGED

};

char *FARNValue[] = {
    "1",   // 0,343728560
    "16",  // 0,297124566
    "31",  // 0,199300763
    "49",  // 0,096384707
    "63",  // 0,048613563
    "95",  // 0,009711077
    "107", // 0,004945561
    "130", // 0,000962156
    "136", // 0,000467035
    "155", // 0,000096792
    "166", // 0,000048396
    "190", // 0,000009780
    "199", // 0,000004514
    "221", // 0,000000878
    "230", // 0,000000376
    "245", // 0,000000119209 (0/129)
    "265", // 0,000000059605 (0/153)
    "286", // 0,000000029802 (0/174)
    "305", // 0,000000014901 (0/205)
    "325", // 0,000000007451 (0/231)
    "345", // 0,000000003725 (0/294)
    "365", // 0,000000001863 (0/362)
    "385", // 0,000000000931 (0/439)
    "405", // 0,000000000466 (0/542)
    "Max", // Maximum possible measure value should be placed here.
    NULL};

char *EnrolTemplValue[] = {"3", "4", "5", "6", "7", "8", "9", "10", NULL};

char *VersionCompValue[] = {"SDK 3.0", "SDK 3.5", "BOTH", NULL};

char *MinuteLevels[] = {"0", "1", "2", "3", "4", NULL};

GtkWidget *topLevelWindow;
GtkWidget *btnCapture;
GtkWidget *btnEnroll;
GtkWidget *btnIdentify;
GtkWidget *btnVerify;
GtkWidget *btnStop;
GtkWidget *btnDelete;

GtkWidget *lbtMessages;
GtkWidget *lbtEnrollProgres;

GdkPixmap *FPFillArea = NULL;
GdkPixbuf *FPPixBuf[4];

GtkWidget *btnckSaveCatureToFile;
DGTBOOL boolSaveCaptureToFile = FALSE;

GtkWidget *btnckMIOTControl;
GtkWidget *btnckFDDControl;
GtkWidget *comboFARSelect;
GtkWidget *comboEnrolTempNumber;
GtkWidget *btnckFastModeCtrl;

GtkWidget *lbtVersionCompatibiliy;
GtkWidget *comboVersionCompSelect;

GtkWidget *comboMinMinuteSelect;
GtkWidget *comboOvelapMinuteSelect;

UDGT32 FPImageSize = 0;
DGT32 FPImageHeight, FPImageWidth;
UDGT32 FPTemplateSize = 0;

void CaptureBtnCallback(GtkWidget *widget, GdkEvent *event, gpointer data);
void EnrollBtnCallback(GtkWidget *widget, GdkEvent *event, gpointer data);
void IdentifyBtnCallback(GtkWidget *widget, GdkEvent *event, gpointer data);
void VerifyBtnCallback(GtkWidget *widget, GdkEvent *event, gpointer data);
void StopBtnCallback(GtkWidget *widget, GdkEvent *event, gpointer data);
void SaveCaptureToFileToggleCallback(GtkWidget *widget, GdkEvent *event,
                                     gpointer data);
void MIOTControlToggleCallback(GtkWidget *widget, GdkEvent *event,
                               gpointer data);
void FDDControlToggleCallback(GtkWidget *widget, GdkEvent *event,
                              gpointer data);
void FastModeCtrlToggleCallback(GtkWidget *widget, GdkEvent *event,
                                gpointer data);

void FARNChangeCallback(GtkWidget *widget, gpointer data);
void MaxTemplChangeCallback(GtkWidget *widget, gpointer data);
void VersionCompChangeCallback(GtkWidget *widget, gpointer data);

void MinMinutChangeCallback(GtkWidget *widget, gpointer data);
void OverlapMinutChangeCallback(GtkWidget *widget, gpointer data);

gint ExposeFPDrawAreaCallback(GtkWidget *widget, GdkEventExpose *event,
                              gpointer data);
gboolean ConfigureFPDrawAreaCallback(GtkWidget *area, GdkEventConfigure *event);

gint eventDelete(GtkWidget *widget, GdkEvent *event, gpointer data);
gint eventDestroy(GtkWidget *widget, GdkEvent *event, gpointer data);

void SetControlState(DGTBOOL Block);

void ShowMessage(char *Message);
char *GetErrorMessage(FTRAPI_RESULT Result);
void ShowError(char *Message);
void ShowErrorByNumber(FTRAPI_RESULT Result);

void FreeResource();

#define MAX_FILE_NAME 256

// biometric operation types
#define BO_CAPTURE 0  // capture operation
#define BO_ENROLL 1   // enroll --"--
#define BO_VERIFY 2   // verify --"--
#define BO_IDENTIFY 3 // identify --"--

typedef struct {
  int ioType; // input/output device. Depend on operatin
  int IsFDDDetect;
  int nTimes;                    // how many operation repete
  int nCurr;                     // current cycle
  char fMask[MAX_FILE_NAME * 2]; // file mask for work with file input/output
} BIOPERPARAMS, *LPBIOPERPARAMS;

DGTBOOL StopAnyOperation = FALSE;

void FTR_CBAPI UserCallBack(FTR_USER_CTX Context, FTR_STATE StateMask,
                            FTR_RESPONSE *pResponse, FTR_SIGNAL Signal,
                            FTR_BITMAP_PTR pBitmap);

void UpdateFPImage(DGTVOID *pImageBuffer);

void *CaptureThread(void *arg);
int SaveImageToBMPFile(char *Name, DGTVOID *pImage);

void *EnrollThread(void *arg);
int SaveTemplateToFile(char *Name, FTR_DATA Template);

void *VerifyThread(void *arg);
int ReadTemplateFromFile(char *Name, FTR_DATA_PTR pTemplate);

void *IdentifyThread(void *arg);

int CheckAndCreateSPDir(char *Name, char *DscIfError);
int GetNameForOperation(int ImgOrTemplate, LPBIOPERPARAMS Params,
                        int ShouldExist);

FTRAPI_RESULT _Res = 0;
#define CHECK_RESULT_EXIT(x)                                                   \
  if ((_Res = x) != FTR_RETCODE_OK) {                                          \
    ShowErrorByNumber(_Res);                                                   \
    FreeResource();                                                            \
    return -1;                                                                 \
  }
#define CHECK_RESULT_SHOW(x)                                                   \
  if ((_Res = x) != FTR_RETCODE_OK) {                                          \
    ShowErrorByNumber(_Res);                                                   \
    return;                                                                    \
  }