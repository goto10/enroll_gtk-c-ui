void focus_in(GtkWidget *widget, gpointer data) { selectedInput = widget; }
void showLogin() {
  gtk_widget_destroy(screensaverLayout);

  GtkWidget *emailLabel;
  GtkWidget *passwordLabel;
  GtkWidget *loginLabel;
  loginLayout = createLayout();

  // labels
  emailLabel = gtk_label_new("Email");
  emailError = gtk_label_new("Please Enter Valid Email Address");
  passwordError = gtk_label_new("Password needs to more than 5 Characters");
  passwordLabel = gtk_label_new("Password");
  loginLabel = gtk_label_new("Login");
  char *labelStyle;
  labelStyle = "<span size='38400'>Login</span>";
  gtk_label_set_markup(GTK_LABEL(loginLabel), labelStyle);
  gtk_widget_show(GTK_WIDGET(emailLabel));
  gtk_widget_show(GTK_WIDGET(passwordLabel));
  gtk_widget_show(GTK_WIDGET(emailError));
  gtk_layout_put(GTK_LAYOUT(loginLayout), emailLabel, 440, 75);
  gtk_layout_put(GTK_LAYOUT(loginLayout), passwordLabel, 440, 200);
  gtk_layout_put(GTK_LAYOUT(loginLayout), loginLabel, 150, 20);
  gtk_layout_put(GTK_LAYOUT(loginLayout), emailError, 440, 155);
  gtk_layout_put(GTK_LAYOUT(loginLayout), passwordError, 440, 275);

  // login button
  loginButton = gtk_button_new_with_label("Login");
  gtk_widget_set_size_request(loginButton, 760, 100);
  gtk_layout_put(GTK_LAYOUT(loginLayout), loginButton, 20, 360);
  gtk_widget_modify_bg(GTK_WIDGET(loginButton), GTK_STATE_NORMAL, &inputColor);
  gtk_widget_modify_bg(GTK_WIDGET(loginButton), GTK_STATE_NORMAL, &inputColor);
  gtk_signal_connect(GTK_OBJECT(loginButton), "button_press_event",
                     GTK_SIGNAL_FUNC(loggedIn), NULL);
  GtkWidget *loginButtonlabel;
  GList *loginButtonlist;
  loginButtonlist = gtk_container_get_children(GTK_CONTAINER(loginButton));
  loginButtonlabel = g_list_nth_data(loginButtonlist, 0);
  gtk_widget_modify_fg(GTK_WIDGET(loginButtonlabel), GTK_STATE_NORMAL,
                       &textColor);
  gtk_label_set_markup(GTK_LABEL(loginButtonlabel), labelStyle);

  // email input
  emailInput = gtk_entry_new_with_max_length(35);
  gtk_widget_show(emailInput);
  gtk_widget_set_size_request(emailInput, 340, 50);
  gtk_layout_put(GTK_LAYOUT(loginLayout), emailInput, 440, 100);
  gtk_widget_modify_base(GTK_WIDGET(emailInput), GTK_STATE_NORMAL, &inputColor);
  gtk_widget_modify_text(GTK_WIDGET(emailInput), GTK_STATE_NORMAL, &textColor);
  g_signal_connect(G_OBJECT(emailInput), "focus-in-event", G_CALLBACK(focus_in),
                   NULL);
  gtk_widget_grab_focus(GTK_WIDGET(emailInput));

  // password input
  passwordInput = gtk_entry_new_with_max_length(35);
  gtk_widget_show(passwordInput);
  gtk_widget_set_size_request(passwordInput, 340, 50);
  gtk_entry_set_invisible_char(GTK_ENTRY(passwordInput), 42);
  gtk_entry_set_visibility(GTK_ENTRY(passwordInput), FALSE);
  gtk_layout_put(GTK_LAYOUT(loginLayout), passwordInput, 440, 225);
  gtk_widget_modify_base(GTK_WIDGET(passwordInput), GTK_STATE_NORMAL,
                         &inputColor);
  gtk_widget_modify_text(GTK_WIDGET(passwordInput), GTK_STATE_NORMAL,
                         &textColor);
  g_signal_connect(G_OBJECT(passwordInput), "focus-in-event",
                   G_CALLBACK(focus_in), NULL);

  // show keyboard
  GtkWidget *keyboard;
  keyboard = nfKeyboard(callbackKeyboard);
  gtk_layout_put(GTK_LAYOUT(loginLayout), keyboard, 20, 75);
  gtk_widget_show(GTK_WIDGET(keyboard));

  // show window
  // gtk_widget_show(GTK_WIDGET(window));
  gtk_widget_show_all(GTK_WIDGET(loginLayout));
}