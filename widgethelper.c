GtkWidget *createButton(char *text, GtkLayout *parent, int w, int h, int x,
                        int y, void(*callback)) {
  GtkWidget *button;
  button = gtk_button_new_with_label(text);
  gtk_widget_set_size_request(GTK_WIDGET(button), w, h);
  gtk_layout_put(GTK_LAYOUT(parent), GTK_WIDGET(button), x, y);
  gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
  gtk_widget_show(GTK_WIDGET(button));
  GtkWidget *label;
  GList *list;
  list = gtk_container_get_children(GTK_CONTAINER(button));
  label = g_list_nth_data(list, 0);
  gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(callback),
                     (gpointer)text);
  return button;
}

GtkWidget *createImage(char *image, GtkLayout *parent, int w, int h, int x,
                       int y, int visible) {
  GtkWidget *img;
  img = gtk_image_new_from_file(image);
  gtk_widget_set_size_request(img, w, h);
  gtk_layout_put(GTK_LAYOUT(parent), img, x, y);
  if (visible == 1) {
    gtk_widget_show(GTK_WIDGET(img));
  }
  return img;
}

GtkWidget *createLabel(char *text, GtkLayout *parent, int w, int h, int x,
                       int y, char *textSize, GdkColor *colour) {
  fflush(stdout);
  GtkWidget *label;
  label = gtk_label_new(text);
  gtk_widget_set_size_request(GTK_WIDGET(label), w, h);
  gtk_layout_put(GTK_LAYOUT(parent), label, x, y);
  char labelStyle[50];
  sprintf(labelStyle, "<span size='%s'>%s</span>", textSize, text);

  gtk_label_set_markup(GTK_LABEL(label), labelStyle);
  gtk_widget_show(GTK_WIDGET(label));
  gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, colour);

  return label;
}

GtkWidget *createClickEvent(GtkLayout *parent, int w, int h, int x, int y,
                            void(*callback)) {
  GtkWidget *event = gtk_event_box_new();
  gtk_widget_set_size_request(GTK_WIDGET(event), w, h);
  gtk_layout_put(GTK_LAYOUT(parent), GTK_WIDGET(event), x, y);
  gtk_event_box_set_visible_window(GTK_EVENT_BOX(event), 0);
  gtk_signal_connect(GTK_OBJECT(event), "button_press_event",
                     GTK_SIGNAL_FUNC(callback), NULL);
  gtk_widget_show(GTK_WIDGET(event));
  return event;
}

void updateLabel(char *text, GtkLabel *label, char *textSize) {
  char *labelStyle;
  // sprintf(labelStyle,, textSize, text);
  labelStyle =
      g_markup_printf_escaped("<span size=\"%s\">%s</span>", textSize, text);
  gtk_label_set_markup(label, labelStyle);
}

GtkWidget *createLayout() {
  GtkWidget *layout = gtk_layout_new(NULL, NULL);
  gtk_container_add(GTK_CONTAINER(window), layout);
  gtk_widget_modify_bg(GTK_WIDGET(layout), GTK_STATE_NORMAL, &bgColor);
  return layout;
}