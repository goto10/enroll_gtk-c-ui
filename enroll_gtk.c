#include <X11/Xlib.h>
#include <fcntl.h>
#include <pthread.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <gtk/gtk.h>
static GdkColor inputColor;
static GdkColor bgColor;
static GdkColor red;
static GdkColor textColor;
static GdkColor black;
static GdkColor green;
static GdkColor amber;
static GtkWidget *selectedInput;
static GtkWidget *passwordInput;
static GtkWidget *emailInput;
static GtkWidget *emailError;
static GtkWidget *passwordError;
static GtkWidget *loginButton;

// #include "ftrscanapi.h"

// #include "ftrapi.h"

// #include "includes.c"
GtkWidget *window;
GtkWidget *screensaverLayout;
GtkWidget *loginLayout;
GtkWidget *loggedinLayout;
GtkWidget *medicalLayout;
GtkWidget *pictureLayout;
GtkWidget *enrollLayout;
GtkWidget *inductionLayout;

void loggedIn();

#include "widgethelper.c"

#include "validation.c"

#include "keyboard.c"

#include "loggedin.c"

#include "login.c"

#include "screensaver.c"

int main(int argc, char *argv[]) {
  gdk_color_parse("#515257", &inputColor);
  gdk_color_parse("#353638", &bgColor);
  gdk_color_parse("red", &red);
  gdk_color_parse("#69d83a", &green);
  gdk_color_parse("orange", &amber);
  gdk_color_parse("white", &textColor);
  gdk_color_parse("#000000", &black);
  XInitThreads();
  // g_thread_init(NULL);
  // gdk_threads_init();
  gtk_init(&argc, &argv);
  gdk_threads_enter();
  showScreenSaver();

  gtk_main();

  gdk_threads_leave();
  return 0;
}
