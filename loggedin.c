#include "ftrapi.h"
#include "init.c"
#include <pthread.h>

#include "enroll.c"
#include "induction.c"
#include "medical.c"
#include "picture.c"

GtkWidget *fpImage;
GtkWidget *createCheckBox(char *text, GtkLayout *parent, int x, int y,
                          int checked, void(*callback)) {
  createClickEvent(parent, 400, 64, 0, y, callback);
  createImage("images/checkbox.png", parent, 64, 64, x, y, 1);
  GtkWidget *check =
      createImage("images/check.png", parent, 64, 64, x, y, checked);
  GtkWidget *checkLabel =
      createLabel(text, GTK_LAYOUT(loggedinLayout), 300, 40, x + 84, y + 10,
                  (char *)"25000", &textColor);
  gtk_misc_set_alignment(GTK_MISC(checkLabel), 0.0f, 0.0f);
  return check;
}

void loggedIn() {
  // gtk_widget_destroy(loginLayout);

  loggedinLayout = createLayout();
  gtk_widget_show(GTK_WIDGET(loggedinLayout));

  GtkWidget *checkMedical = createCheckBox(
      "Medical", GTK_LAYOUT(loggedinLayout), 10, 20, 0, showMedical);
  GtkWidget *checkEnroll = createCheckBox(
      "Enroll Fingerprint", GTK_LAYOUT(loggedinLayout), 10, 110, 0, showEnroll);
  GtkWidget *checkPicture = createCheckBox(
      "Profile Picture", GTK_LAYOUT(loggedinLayout), 10, 200, 0, showPicture);
  GtkWidget *checkInduction = createCheckBox(
      "Induction", GTK_LAYOUT(loggedinLayout), 10, 300, 0, showInduction);

  gtk_widget_show(GTK_WIDGET(checkMedical));
  gtk_widget_show(GTK_WIDGET(checkEnroll));
  gtk_widget_show(GTK_WIDGET(checkPicture));
  gtk_widget_show(GTK_WIDGET(checkInduction));
  // gdk_flush ();
}