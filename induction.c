
void inductionPicture() {
  gtk_widget_destroy(GTK_WIDGET(inductionLayout));
  loggedIn();
}

void showInduction() {
  gtk_widget_destroy(GTK_WIDGET(loggedinLayout));
  inductionLayout = createLayout();
  GtkWidget *titleLabel = gtk_label_new("Induction");
  gtk_layout_put(GTK_LAYOUT(inductionLayout), GTK_WIDGET(titleLabel), 0, 0);
  gtk_widget_show(GTK_WIDGET(titleLabel));
  // back button
  createImage("images/back.png", GTK_LAYOUT(inductionLayout), 64, 64, 0, 0, 1);
  createClickEvent(GTK_LAYOUT(inductionLayout), 64, 64, 0, 0, inductionPicture);
  // show layout
  gtk_widget_show(GTK_WIDGET(inductionLayout));
}
