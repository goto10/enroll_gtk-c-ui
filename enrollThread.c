int frameNumber;
void *EnrollThread(void *arg) {

  frameNumber = 0;
  DGTVOID *pTemplate;
  FTRAPI_RESULT Res = 0;
  LPBIOPERPARAMS lpBOP = (LPBIOPERPARAMS)arg;
  FTR_DATA Template;
  FTR_ENROLL_DATA eData;
  DGTBOOL vResult;
  FTR_FARN vFARN;
  gdk_threads_enter();

  pTemplate = malloc(FPTemplateSize);

  if (pTemplate) {
    Template.dwSize = FPTemplateSize;
    Template.pData = pTemplate;
    eData.dwSize = sizeof(FTR_ENROLL_DATA);

    Res =
        FTREnrollX((FTR_USER_CTX)lpBOP, FTR_PURPOSE_ENROLL, &Template, &eData);
  }

  if (!pTemplate) {

    // ShowMessage( "No memory for template" );
  } else {
    if (Res != FTR_RETCODE_OK) {
      if (lpBOP && lpBOP->IsFDDDetect) {

        // ShowMessage( "Fake finger detected" );
      } else {

        // ShowMessage( GetErrorMessage( Res ) );
      }
    } else {
      char cQuality[64];
      char msg[MAX_FILE_NAME];

      memset(msg, 0, sizeof(msg));
      memset(cQuality, 0, sizeof(cQuality));
      strcat(msg, "Successfull enroll. Quality ");
      sprintf(cQuality, "%d of 10", (int)eData.dwQuality);
      strcat(msg, cQuality);

      // ShowMessage( msg );

      if (pTemplate) {

        Res = FTRVerifyN("verify", (FTR_DATA_PTR)&Template, &vResult, &vFARN);

        if (vResult) {
          // success
          printf("verified success: %ld\n", vFARN);
          gtk_widget_modify_bg(GTK_WIDGET(fpFrame), GTK_STATE_NORMAL, &green);
          updateLabel("Status: SUCCESS", GTK_LABEL(enrollmentLabel), "14000");
          gtk_widget_hide(GTK_WIDGET(instructionLabel));
          createImage("images/fpsuccess.gif", GTK_LAYOUT(enrollLayout), 230,
                      227, 500, 150, 1);
          int milliseconds = 5000;
          if (milliseconds <= 0) {
            return NULL;
          }
          int milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
          int end = milliseconds_since + milliseconds;
          do {
            milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
          } while (milliseconds_since <= end);
          closeEnroll();
        } else {
          printf("verified failed mofo\n");
        }
      }
    }
  }

  // SetControlState( FALSE );

  gdk_flush();
  gdk_threads_leave();

  if (lpBOP) {
    free(lpBOP);
  }

  if (pTemplate) {
    free(pTemplate);
  }
  pthread_exit(0);
  return NULL;
}