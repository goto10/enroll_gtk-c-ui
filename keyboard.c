#include "nfkeys.c"

static GtkWidget *keys[100];

static int keyboardModifier;
GtkWidget *lowerKeys;

GtkWidget *shiftKeys;
GtkWidget *symbolKeys;

void shiftPress(GtkWidget *widget, gpointer data) {

  GtkWidget *shiftButtonlabel;
  GList *shiftButtonlist;
  shiftButtonlist = gtk_container_get_children(GTK_CONTAINER(widget));
  shiftButtonlabel = g_list_nth_data(shiftButtonlist, 0);

  // printf("key pressed %s\n",(char *) data);

  switch (keyboardModifier) {
  case 1:
    gtk_label_set_text(GTK_LABEL(shiftButtonlabel), "$#&");

    gtk_widget_hide_all(GTK_WIDGET(symbolKeys));
    gtk_widget_show_all(GTK_WIDGET(shiftKeys));
    gtk_widget_hide_all(GTK_WIDGET(lowerKeys));
    keyboardModifier = 2;
    break;
  case 2:
    gtk_label_set_text(GTK_LABEL(shiftButtonlabel), "abc");

    gtk_widget_show_all(GTK_WIDGET(symbolKeys));
    gtk_widget_hide_all(GTK_WIDGET(shiftKeys));
    gtk_widget_hide_all(GTK_WIDGET(lowerKeys));
    keyboardModifier = 3;
    break;
  case 3:
    gtk_label_set_text(GTK_LABEL(shiftButtonlabel), "ABC");

    gtk_widget_hide_all(GTK_WIDGET(symbolKeys));
    gtk_widget_hide_all(GTK_WIDGET(shiftKeys));
    gtk_widget_show_all(GTK_WIDGET(lowerKeys));
    keyboardModifier = 1;
    break;
  }
}

void backspacePress(GtkWidget *widget, gpointer data) {
  GtkEntryBuffer *existing = gtk_entry_get_buffer(GTK_ENTRY(selectedInput));
  int existingLength = gtk_entry_buffer_get_length(existing);

  gtk_entry_buffer_delete_text(existing, existingLength - 1, -1);

  gtk_entry_set_buffer(GTK_ENTRY(selectedInput), existing);

  validation();
}

GtkWidget *nfKeyboard(void *callback) {

  keyboardModifier = 1;
  GtkWidget *box;

  GtkWidget *shiftButton;
  GtkWidget *spaceButton;
  GtkWidget *mailButton[4];
  GtkWidget *backspaceButton;

  box = gtk_fixed_new();
  int i;
  int a;
  int s;

  // number/letter/symbols

  for (a = 0; a < 10; a++) {
    int newval = 0;
    newval += a;
    keys[newval] = gtk_fixed_new();
    GtkWidget *button;
    button = gtk_button_new_with_label(key[newval]);
    gtk_widget_set_size_request(button, 40, 40);
    gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
    GtkWidget *label;
    GList *list;
    list = gtk_container_get_children(GTK_CONTAINER(button));
    label = g_list_nth_data(list, 0);
    gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

    gtk_container_add(GTK_CONTAINER(keys[newval]), button);
    gtk_fixed_put(GTK_FIXED(box), keys[newval], a * 40, 0);
    gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(callback),
                       (gpointer)key[newval]);
    gtk_widget_show_all(keys[newval]);
  }

  {
    lowerKeys = gtk_fixed_new();
    for (i = 1; i < 4; i++) {
      for (s = 0; s < 10; s++) {
        int newval = 0;
        newval += i;
        newval *= 10;
        newval += s;

        keys[newval] = gtk_fixed_new();
        GtkWidget *button;
        button = gtk_button_new_with_label(key[newval]);
        gtk_widget_set_size_request(button, 40, 40);
        gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
        GtkWidget *label;
        GList *list;
        list = gtk_container_get_children(GTK_CONTAINER(button));
        label = g_list_nth_data(list, 0);
        gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

        gtk_container_add(GTK_CONTAINER(keys[newval]), button);
        gtk_fixed_put(GTK_FIXED(lowerKeys), keys[newval], s * 40, i * 40);
        gtk_signal_connect(GTK_OBJECT(button), "clicked",
                           GTK_SIGNAL_FUNC(callback), (gpointer)key[newval]);
      }
    }
    gtk_fixed_put(GTK_FIXED(box), lowerKeys, 0, 0);
    gtk_widget_show_all(GTK_WIDGET(lowerKeys));
  }

  // shift press
  {

    shiftKeys = gtk_fixed_new();
    for (i = 1; i < 4; i++) {

      for (s = 0; s < 10; s++) {
        int newval = 0;
        newval += i;
        newval *= 10;
        newval += s;
        newval += 30;
        fflush(stdout);
        keys[newval] = gtk_fixed_new();
        GtkWidget *button;
        button = gtk_button_new_with_label(key[newval]);
        gtk_widget_set_size_request(button, 40, 40);
        gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
        GtkWidget *label;
        GList *list;
        list = gtk_container_get_children(GTK_CONTAINER(button));
        label = g_list_nth_data(list, 0);
        gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

        gtk_container_add(GTK_CONTAINER(keys[newval]), button);
        gtk_fixed_put(GTK_FIXED(shiftKeys), keys[newval], s * 40, i * 40);
        gtk_signal_connect(GTK_OBJECT(button), "clicked",
                           GTK_SIGNAL_FUNC(callback), (gpointer)key[newval]);
        // gtk_widget_show_all(shiftKeys);
      }
    }
    gtk_fixed_put(GTK_FIXED(box), shiftKeys, 0, 0);
  }

  // symbols
  {
    symbolKeys = gtk_fixed_new();
    for (i = 1; i < 4; i++) {

      for (s = 0; s < 10; s++) {
        int newval = 0;
        newval += i;
        newval *= 10;
        newval += s;
        newval += 60;

        keys[newval] = gtk_fixed_new();
        GtkWidget *button;
        button = gtk_button_new_with_label(key[newval]);
        gtk_widget_set_size_request(button, 40, 40);
        gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
        GtkWidget *label;
        GList *list;
        list = gtk_container_get_children(GTK_CONTAINER(button));
        label = g_list_nth_data(list, 0);
        gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

        gtk_container_add(GTK_CONTAINER(keys[newval]), button);
        gtk_fixed_put(GTK_FIXED(symbolKeys), keys[newval], s * 40, i * 40);
        gtk_signal_connect(GTK_OBJECT(button), "clicked",
                           GTK_SIGNAL_FUNC(callback), (gpointer)key[newval]);
        // gtk_widget_show_all(keys[newval]);
      }
    }
    gtk_fixed_put(GTK_FIXED(box), symbolKeys, 0, 0);
  }

  {
    // email helper
    int emailIt;
    for (emailIt = 0; emailIt < 4; emailIt++) {
      mailButton[emailIt] = gtk_fixed_new();

      GtkWidget *button;
      button = gtk_button_new_with_label(emailHelper[emailIt]);
      gtk_widget_set_size_request(button, 100, 40);
      gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
      GtkWidget *label;
      GList *list;
      list = gtk_container_get_children(GTK_CONTAINER(button));
      label = g_list_nth_data(list, 0);
      gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

      gtk_container_add(GTK_CONTAINER(mailButton[emailIt]), button);
      gtk_fixed_put(GTK_FIXED(box), mailButton[emailIt], emailIt * 100, 160);
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(callback),
                         (gpointer)emailHelper[emailIt]);
      gtk_widget_show_all(GTK_WIDGET(mailButton[emailIt]));
    }
  }
  // shift/symbols/space/backspace

  {
    GtkWidget *button;
    shiftButton = gtk_fixed_new();

    button = gtk_button_new_with_label("ABC");
    gtk_widget_set_size_request(GTK_WIDGET(button), 100, 40);
    gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
    GtkWidget *label;
    GList *list;
    list = gtk_container_get_children(GTK_CONTAINER(button));
    label = g_list_nth_data(list, 0);
    gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

    gtk_container_add(GTK_CONTAINER(shiftButton), GTK_WIDGET(button));
    gtk_fixed_put(GTK_FIXED(box), GTK_WIDGET(shiftButton), 0, 200);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
                       GTK_SIGNAL_FUNC(shiftPress), (gpointer) "SHIFT");
    gtk_widget_show_all(GTK_WIDGET(shiftButton));
  }
  // space

  {
    GtkWidget *button;
    spaceButton = gtk_fixed_new();

    button = gtk_button_new_with_label("space");
    gtk_widget_set_size_request(GTK_WIDGET(button), 200, 40);
    gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
    GtkWidget *label;
    GList *list;
    list = gtk_container_get_children(GTK_CONTAINER(button));
    label = g_list_nth_data(list, 0);
    gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

    gtk_container_add(GTK_CONTAINER(spaceButton), button);
    gtk_fixed_put(GTK_FIXED(box), spaceButton, 100, 200);
    gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(callback),
                       (gpointer) " ");
    gtk_widget_show_all(GTK_WIDGET(spaceButton));
  }
  //  //backspace
  {
    GtkWidget *button;
    backspaceButton = gtk_fixed_new();

    button = gtk_button_new_with_label("DELETE");
    gtk_widget_set_size_request(GTK_WIDGET(button), 100, 40);
    gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &inputColor);
    GtkWidget *label;
    GList *list;
    list = gtk_container_get_children(GTK_CONTAINER(button));
    label = g_list_nth_data(list, 0);
    gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &textColor);

    gtk_container_add(GTK_CONTAINER(backspaceButton), button);
    gtk_fixed_put(GTK_FIXED(box), backspaceButton, 300, 200);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
                       GTK_SIGNAL_FUNC(backspacePress), (gpointer) "backspace");
    gtk_widget_show_all(GTK_WIDGET(backspaceButton));
  }
  // gtk_widget_show(box);
  //  gtk_widget_hide_all(symbolKeys);

  return box;
}

void callbackKeyboard(GtkWidget *widget, gpointer data) {
  printf("key pressed \n");

  gtk_entry_append_text(GTK_ENTRY(selectedInput), data);

  validation();
}