int match(char *string, char *pattern) {
  regex_t re;
  if (regcomp(&re, pattern, REG_EXTENDED | REG_NOSUB) != 0)
    return 0;
  int status = regexec(&re, string, 0, NULL, 0);
  regfree(&re);
  if (status != 0)
    return 0;
  return 1;
}

int isValidEmail() {
  char *emailString = (char *)gtk_entry_get_text(GTK_ENTRY(emailInput));
  int valid =
      match(emailString,
            "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
  if (valid == 1) {
    gtk_widget_hide(GTK_WIDGET(emailError));
    return 1;
  } else {
    gtk_widget_show(GTK_WIDGET(emailError));
    return 0;
  }
}

int isValidPassword() {
  char *passwordString = (char *)gtk_entry_get_text(GTK_ENTRY(passwordInput));
  int valid = strlen(passwordString);
  if (valid > 4) {
    gtk_widget_hide(passwordError);
    return 1;
  } else {
    gtk_widget_show(passwordError);
    return 0;
  }
}

void validation() {
  int validE = isValidEmail();
  int validP = isValidPassword();
  if (validE == 1 && validP == 1) {
    gtk_widget_show(GTK_WIDGET(loginButton));
  } else {
    gtk_widget_hide(GTK_WIDGET(loginButton));
  }
}