
TARGET=enroll_gtk
CFLAGS= -g -Wall -O2 -I./
APP_DLIB= -I./Include ./ftrapi.so ./libScanAPI.so `pkg-config gtk+-2.0 --libs` `pkg-config gtk+-2.0 --cflags` -lgthread-2.0 -lX11
all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c $(APP_DLIB)

clean:
	rm -f $(TARGET)
