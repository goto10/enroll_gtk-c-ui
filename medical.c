
void closeMedical() {
  gtk_widget_destroy(GTK_WIDGET(medicalLayout));
  loggedIn();
}

void showMedical() {
  gtk_widget_destroy(GTK_WIDGET(loggedinLayout));
  medicalLayout = createLayout();
  GtkWidget *titleLabel = gtk_label_new("Medical");
  gtk_layout_put(GTK_LAYOUT(medicalLayout), GTK_WIDGET(titleLabel), 0, 0);
  gtk_widget_show(GTK_WIDGET(titleLabel));
  // back button
  createImage("images/back.png", GTK_LAYOUT(medicalLayout), 64, 64, 0, 0, 1);
  createClickEvent(GTK_LAYOUT(medicalLayout), 64, 64, 0, 0, closeMedical);
  // show window
  gtk_widget_show(GTK_WIDGET(medicalLayout));
}