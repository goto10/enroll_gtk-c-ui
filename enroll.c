#include <gdk-pixbuf/gdk-pixbuf.h>
GtkWidget *statusLabel;
GtkWidget *imageFrames[4];
GtkWidget *instructionLabel;
GtkWidget *fpFrame;
GtkWidget *enrollmentLabel;
GtkWidget *fpImage;
#include <time.h>
void closeEnroll();

#include "enrollThread.c"

void scanComplete() {

  int milliseconds = 3000;
  if (milliseconds <= 0) {
    return;
  }
  int milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
  int end = milliseconds_since + milliseconds;
  do {
    milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
  } while (milliseconds_since <= end);
  updateLabel("Please remove finger", GTK_LABEL(instructionLabel), "14000");
  gtk_widget_show_all(GTK_WIDGET(imageFrames[frameNumber]));
  gtk_widget_modify_bg(GTK_WIDGET(imageFrames[frameNumber]), GTK_STATE_NORMAL,
                       &green);
  gtk_widget_hide(GTK_WIDGET(fpImage));
  if (frameNumber == 3) {
    gtk_widget_modify_bg(GTK_WIDGET(fpFrame), GTK_STATE_NORMAL, &amber);
    updateLabel("Status: VERIFICATION", GTK_LABEL(enrollmentLabel), "14000");
    updateLabel("Scan again to verify fingerprint", GTK_LABEL(instructionLabel),
                "14000");
  }
}

void FTR_CBAPI scannerCallback(FTR_USER_CTX Context, FTR_STATE StateMask,
                               FTR_RESPONSE *pResponse, FTR_SIGNAL Signal,
                               FTR_BITMAP_PTR pBitmap) {
  // gdk_threads_enter ();
  LPBIOPERPARAMS lpboc = (LPBIOPERPARAMS)Context; // biometric operation context
  FTR_PROGRESS *lpPrgData;                        // current progress data
  char prgTitle[MAX_FILE_NAME];                   // progress window text
  lpPrgData = (FTR_PROGRESS *)pResponse;

  //
  char *ctx = (char *)Context;
  char *verify = "verify";
  // frame show
  GdkPixbuf *tmpFPPixBuf;
  if ((StateMask & FTR_STATE_FRAME_PROVIDED) && (ctx != verify)) {
    gtk_widget_show(GTK_WIDGET(fpImage));
    tmpFPPixBuf = gdk_pixbuf_new_from_data(
        (DGTVOID *)pBitmap->Bitmap.pData, GDK_COLORSPACE_RGB, FALSE, 8,
        FPImageWidth, FPImageHeight, FPImageWidth * 1, NULL, NULL);
    GdkPixbuf *tmpFPPixBuf2 = gdk_pixbuf_new_subpixbuf(
        tmpFPPixBuf, 0, 0, FPImageWidth / 3, FPImageHeight);
    GdkPixbuf *tmpFPPixBuf3 =
        gdk_pixbuf_scale_simple(tmpFPPixBuf2, 80, 120, GDK_INTERP_NEAREST);
    GtkWidget *tmpImage = gtk_image_new_from_pixbuf(tmpFPPixBuf3);
    gtk_container_add(GTK_CONTAINER(imageFrames[frameNumber]), tmpImage);
    // g_object_unref(G_OBJECT(tmpFPPixBuf));
    // g_object_unref(G_OBJECT(tmpFPPixBuf2));
    // g_object_unref(G_OBJECT(tmpFPPixBuf3));

    scanComplete();
    frameNumber++;
  }

  if ((StateMask & FTR_STATE_SIGNAL_PROVIDED)) {
    switch (Signal) {
    case FTR_SIGNAL_TOUCH_SENSOR:
      updateLabel("Put your finger on the scanner", GTK_LABEL(instructionLabel),
                  "14000");

      break;

    case FTR_SIGNAL_TAKE_OFF:
      if (lpboc && lpboc->ioType == BO_ENROLL) {

        // update progress
        sprintf(prgTitle, "Enroll %d of %d", (int)lpPrgData->dwCount,
                (int)lpPrgData->dwTotal);
        // gtk_label_set_text ((GtkLabel *) lbtEnrollProgres, prgTitle);
      }

      break;

    case FTR_SIGNAL_FAKE_SOURCE: {

      if (lpboc) {
        printf("fake sauce\n");

        lpboc->IsFDDDetect = TRUE;
      }

      break;
    }
    case FTR_SIGNAL_UNDEFINED:
      printf("Undefined signal value\n");
      break;
    } // switch( Signal )
  }

  if (StopAnyOperation == FALSE)
    *pResponse = FTR_CONTINUE;
  else
    *pResponse = FTR_CANCEL;

  if (lpboc && lpboc->IsFDDDetect) {
    *pResponse = FTR_CANCEL;
  }
  fflush(stdout);

  gdk_flush();
  // gdk_threads_leave ();
}

void closeEnroll() {

  gtk_widget_destroy(GTK_WIDGET(enrollLayout));

  loggedIn();
}

int initScanner() {
  int FTR = FTRInitialize();
  if (FTR == 0) {
    FTRSetParam(FTR_PARAM_CB_FRAME_SOURCE, (FTR_PARAM_VALUE)FSD_FUTRONIC_USB);
    FTRSetParam(FTR_PARAM_CB_CONTROL, (FTR_PARAM_VALUE)scannerCallback);
    FTRSetParam(FTR_PARAM_MAX_FARN_REQUESTED, (FTR_PARAM_VALUE)166);
    FTRSetParam(FTR_PARAM_FAKE_DETECT, (FTR_PARAM_VALUE)FALSE);
    FTRSetParam(FTR_PARAM_FFD_CONTROL, (FTR_PARAM_VALUE)FALSE);
    FTRSetParam(FTR_PARAM_MIOT_CONTROL, (FTR_PARAM_VALUE)FALSE);
    FTRSetParam(FTR_PARAM_MAX_MODELS, (FTR_PARAM_VALUE)10);
    FTRSetParam(FTR_PARAM_FAST_MODE, (FTR_PARAM_VALUE)FALSE);
    FTRSetParam(FTR_PARAM_MIN_MINUT_LEVEL, (FTR_PARAM_VALUE)3);
    FTRSetParam(FTR_PARAM_OVERLAP_LEVEL, (FTR_PARAM_VALUE)3);
    FTRSetParam(FTR_PARAM_MAX_MODELS, (FTR_PARAM_VALUE)4);
    FTRGetParam(FTR_PARAM_IMAGE_SIZE, (FTR_PARAM_VALUE *)&FPImageSize);
    FTRGetParam(FTR_PARAM_IMAGE_WIDTH, (FTR_PARAM_VALUE)&FPImageWidth);
    FTRGetParam(FTR_PARAM_IMAGE_HEIGHT, (FTR_PARAM_VALUE)&FPImageHeight);
    FTRGetParam(FTR_PARAM_MAX_TEMPLATE_SIZE, (FTR_PARAM_VALUE)&FPTemplateSize);

    // for(int i =0;i<4;i++){
    //                    daFPDrawArea[i] = gtk_drawing_area_new();
    //                    gtk_signal_connect((GtkObject * ) daFPDrawArea[i],
    //                    "expose_event", (GtkSignalFunc)
    //                    ExposeFPDrawAreaCallback, NULL);
    //                    gtk_signal_connect((GtkObject * ) daFPDrawArea[i],
    //                    "configure_event", (GtkSignalFunc)
    //                    ConfigureFPDrawAreaCallback, NULL);
    //                    gtk_drawing_area_size((GtkDrawingArea * )
    //                    daFPDrawArea[i], 100, 150);
    //                    gtk_layout_put(GTK_LAYOUT(enrollLayout),
    //                    daFPDrawArea[i], 100*i, 64);
    //                    gtk_widget_show(GTK_WIDGET(daFPDrawArea[i]));
    // }
  }
  return FTR;
}

void showEnroll() {

  gtk_widget_destroy(GTK_WIDGET(loggedinLayout));
  enrollLayout = createLayout();

  // show window
  gtk_widget_show(GTK_WIDGET(enrollLayout));

  initScanner();

  createImage("images/fpbg.png", GTK_LAYOUT(enrollLayout), 230, 300, 500, 90,
              1);

  fpImage = gtk_image_new_from_file("images/fp.gif");
  gtk_widget_set_size_request(fpImage, 200, 282);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fpImage, 515, 99);

  int offsetx = 70;
  int offsety = 70;

  fpFrame = gtk_frame_new(NULL);
  gtk_widget_set_size_request(fpFrame, 264, 344);
  gtk_widget_modify_bg(GTK_WIDGET(fpFrame), GTK_STATE_NORMAL, &red);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fpFrame, offsetx, offsety);
  gtk_widget_show(GTK_WIDGET(fpFrame));

  GtkWidget *fp1;
  fp1 = gtk_frame_new(NULL);
  gtk_widget_set_size_request(fp1, 82, 122);
  gtk_widget_modify_bg(GTK_WIDGET(fp1), GTK_STATE_NORMAL, &black);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fp1, offsetx + 40, offsety + 40);
  gtk_widget_show(GTK_WIDGET(fp1));
  imageFrames[0] = fp1;

  GtkWidget *fp2;
  fp2 = gtk_frame_new(NULL);
  gtk_widget_set_size_request(fp2, 82, 122);
  gtk_widget_modify_bg(GTK_WIDGET(fp2), GTK_STATE_NORMAL, &black);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fp2, offsetx + 102 + 40,
                 offsety + 40);
  gtk_widget_show(GTK_WIDGET(fp2));
  imageFrames[1] = fp2;

  GtkWidget *fp3;
  fp3 = gtk_frame_new(NULL);
  gtk_widget_set_size_request(fp3, 82, 122);
  gtk_widget_modify_bg(GTK_WIDGET(fp3), GTK_STATE_NORMAL, &black);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fp3, offsetx + 40,
                 offsety + 142 + 40);
  gtk_widget_show(GTK_WIDGET(fp3));
  imageFrames[2] = fp3;

  GtkWidget *fp4;

  fp4 = gtk_frame_new(NULL);
  gtk_widget_set_size_request(fp4, 82, 122);
  gtk_widget_modify_bg(GTK_WIDGET(fp4), GTK_STATE_NORMAL, &black);
  gtk_layout_put(GTK_LAYOUT(enrollLayout), fp4, offsetx + 102 + 40,
                 offsety + 142 + 40);
  gtk_widget_show(GTK_WIDGET(fp4));
  imageFrames[3] = fp4;

  enrollmentLabel = createLabel("Status: ENROLMENT", GTK_LAYOUT(enrollLayout),
                                400, 60, 0, 20, "14000", &textColor);

  instructionLabel =
      createLabel("Put your finger on the scanner", GTK_LAYOUT(enrollLayout),
                  400, 60, 410, 50, "14000", &textColor);
  // gtk_widget_modify_fg (GTK_WIDGET(instructionLabel), GTK_STATE_NORMAL,
  // &textColor);

  pthread_t thread_id;
  pthread_create(&thread_id, NULL, EnrollThread, NULL);

  // gdk_flush ();
}
