

void showScreenSaver() {
  GtkWidget *logo;
  GtkWidget *touch;
  GtkWidget *box;
  GtkWidget *eventBoxLogo;

  box = gtk_fixed_new();
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_window_set_default_size(GTK_WINDOW(window), 800, 480);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

  screensaverLayout = gtk_layout_new(NULL, NULL);
  gtk_container_add(GTK_CONTAINER(window), screensaverLayout);
  gtk_widget_show(GTK_WIDGET(screensaverLayout));

  gtk_window_set_decorated(GTK_WINDOW(window), FALSE);

  eventBoxLogo = gtk_event_box_new();
  gtk_widget_set_size_request(GTK_WIDGET(eventBoxLogo), 800, 480);
  gtk_layout_put(GTK_LAYOUT(screensaverLayout), eventBoxLogo, 0, 0);
  gtk_widget_set_events(GTK_WIDGET(eventBoxLogo), GDK_BUTTON_PRESS_MASK);
  gtk_signal_connect(GTK_OBJECT(eventBoxLogo), "button_press_event",
                     GTK_SIGNAL_FUNC(showLogin), NULL);

  gtk_container_add(GTK_CONTAINER(eventBoxLogo), box);

  logo = gtk_image_new_from_file("images/gym.png");
  gtk_container_add(GTK_CONTAINER(box), logo);

  touch = gtk_image_new_from_file("images/touch.png");
  gtk_container_add(GTK_CONTAINER(box), touch);

  g_signal_connect_swapped(G_OBJECT(window), "destroy",
                           G_CALLBACK(gtk_main_quit), NULL);

  gtk_widget_show_all(GTK_WIDGET(window));

  gtk_widget_destroy(GTK_WIDGET(screensaverLayout));
  loggedIn(); // temp
}